"""
Check Python style with pycodestyle, pydocstyle and pylint.

EECS 485 Project 1

Andrew DeOrio <awdeorio@umich.edu>
"""
import sh


def test_pycodestyle():
    """Run `pycodestyle setup.py insta485generator`."""
    check_for_prohibited_terms()
    pycodestyle = sh.Command("pycodestyle")
    print("\n", pycodestyle, pycodestyle("--version"))
    output = pycodestyle("setup.py", "insta485generator")
    assert output.exit_code == 0, str(output)


def test_pydocstyle():
    """Run `pydocstyle setup.py insta485generator`."""
    check_for_prohibited_terms()
    pydocstyle = sh.Command("pydocstyle")
    print("\n", pydocstyle, pydocstyle("--version"))
    output = pydocstyle("setup.py", "insta485generator")
    assert output.exit_code == 0, str(output)


def test_pylint():
    """Run pylint."""
    check_for_prohibited_terms()
    pylint = sh.Command("pylint")
    print("\n", pylint, pylint("--version"))
    output = pylint(
        "--disable=no-value-for-parameter",
        "setup.py",
        "insta485generator",
    )
    assert output.exit_code == 0, str(output)


def check_for_prohibited_terms():
    """Check for prohibited terms in student solution."""
    prohibited_terms = ['nopep8', 'noqa', 'pylint']
    for prohibited_term in prohibited_terms:
        print(prohibited_term)
        grep = sh.Command("grep")
        try:
            output = grep(
                "-r",
                "-n",
                prohibited_term,
                "--include=*.py",
                "insta485generator"
            )
        except sh.ErrorReturnCode_1:
            # Did not find prohibited term, do nothing
            pass
        else:
            # Zero exit code indicates that grep found a prohibited term.
            assert output.exit_code != 0, (
                "The term '{}' is prohibited. "
                "Do NOT disable style checks.\n{}".format(
                    prohibited_term, str(output))
            )
