"""
Validate handcoded HTML against HTML5 W3C spec.

EECS 485 Project 1

Andrew DeOrio <awdeorio@umich.edu>
"""
import os
import sh


def test_html():
    """Validate handcoded HTML5 in html/."""
    assert os.path.isfile("html/index.html")
    assert os.path.isfile("html/u/awdeorio/index.html")
    sh.html5validator("--root", "html", "--ignore", "JAVA_TOOL_OPTIONS")
