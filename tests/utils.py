"""P2 autograder utility functions."""
import os


# Directory containing unit tests
TEST_DIR = os.path.dirname(__file__)


def get_input_dir(basename):
    """Return absolute path of input directory."""
    if os.path.basename(TEST_DIR) == "autograder":
        # In the autograder environment, inputs are in autograder/testdata/
        return os.path.join(TEST_DIR, "testdata", basename)

    # In the student environment, look for inputs in the project root,
    # which we assume to be the present working directory.
    return basename


def get_output_dir(basename):
    """Return absolute path of html output directory."""
    if os.path.basename(TEST_DIR) == "autograder":
        # Autograder environment example: autograder/testdata/<basename>/html
        return os.path.join(TEST_DIR, "testdata", basename, "html")

    # Student environment example: <basename>/html
    return os.path.join(basename, "html")
