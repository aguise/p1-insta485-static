"""
Validate generated HTML against HTML5 W3C spec.

EECS 485 Project 1

Andrew DeOrio <awdeorio@umich.edu>
"""
import os
import shutil
import sh


def test_html():
    """Validate generated HTML5 in insta485/html/ ."""
    assert os.path.isdir("insta485")
    shutil.rmtree("insta485/html", ignore_errors=True)
    sh.insta485generator("insta485")

    # Verify expected files are present
    assert os.path.isfile("insta485/html/index.html")
    assert os.path.isfile(
        # Drew
        "insta485/html/uploads/e1a7c5c32973862ee15173b0259e3efdb6a391af.jpg"
    )
    assert os.path.isfile(
        # Jag
        "insta485/html/uploads/73ab33bd357c3fd42292487b825880958c595655.jpg"
    )
    assert os.path.isfile(
        # Mike
        "insta485/html/uploads/5ecde7677b83304132cb2871516ea50032ff7a4f.jpg"
    )
    assert os.path.isfile(
        # Jason
        "insta485/html/uploads/505083b8b56c97429a728b68f31b0b2a089e5113.jpg"
    )
    assert os.path.isfile(
        # Post 1
        "insta485/html/uploads/122a7d27ca1d7420a1072f695d9290fad4501a41.jpg"
    )
    assert os.path.isfile(
        # Post 2
        "insta485/html/uploads/ad7790405c539894d25ab8dcf0b79eed3341e109.jpg"
    )
    assert os.path.isfile(
        # Post 3
        "insta485/html/uploads/9887e06812ef434d291e4936417d125cd594b38a.jpg"
    )
    assert os.path.isfile(
        # Post 4
        "insta485/html/uploads/2ec7cf8ae158b3b1f40065abfb33e81143707842.jpg"
    )

    # Verify HTML5
    sh.html5validator(
        "--root", "insta485/html",
        "--ignore", "JAVA_TOOL_OPTIONS",
    )
