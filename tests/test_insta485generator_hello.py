"""
Test insta485generator with published "hello" input.

EECS 485 Project 1

Andrew DeOrio <awdeorio@umich.edu>
"""
import os
import re
import shutil
import textwrap
import sh
import utils


def test_index():
    """Diff check hello/index.html."""
    input_dir = utils.get_input_dir("hello")
    output_dir = utils.get_output_dir("hello")
    shutil.rmtree(output_dir, ignore_errors=True)
    sh.insta485generator(input_dir)

    # Make sure generated files exist
    filename = os.path.join(output_dir, "index.html")
    assert os.path.isdir(output_dir)
    assert os.path.isfile(filename)

    # Verify output file content, normalized for whitespace
    with open(filename) as infile:
        actual = infile.read()
    correct = textwrap.dedent("""
        <!DOCTYPE html>
        <html lang="en">
           <head>
             <title>
               Hello world
             </title>
           </head>
           <body>
             hello
             world
           </body>
        </html>
    """)
    correct = re.sub(r"\s+", "", correct)
    actual = re.sub(r"\s+", "", actual)
    assert actual == correct
