"""
Test insta485generator with published "hello_css" input.

EECS 485 Project 1

Andrew DeOrio <awdeorio@umich.edu>
"""
import os
import re
import shutil
import textwrap
import sh
import utils


def test_index():
    """Diff check hello/index.html."""
    input_dir = utils.get_input_dir("hello_css")
    output_dir = utils.get_output_dir("hello_css")
    shutil.rmtree(output_dir, ignore_errors=True)
    sh.insta485generator(input_dir)

    # Make sure generated files exist
    output_html = os.path.join(output_dir, "index.html")
    output_css = os.path.join(output_dir, "css/style.css")
    assert os.path.isdir(output_dir)
    assert os.path.isfile(output_html)
    assert os.path.isfile(output_css)

    # Verify output file content, normalized for whitespace
    with open(output_html) as infile:
        actual_html = infile.read()
    correct_html = textwrap.dedent("""
        <!DOCTYPE html>
        <html lang="en">
          <head>
             <title>Hello world</title>
             <link rel="stylesheet" type="text/css" href="/css/style.css">
          </head>
          <body>
            <div class="important">hello</div>
            <div class="important">world</div>
          </body>
        </html>
    """)
    correct_html = re.sub(r"\s+", "", correct_html)
    actual_html = re.sub(r"\s+", "", actual_html)
    assert actual_html == correct_html

    # Verify CSS content
    correct_css = textwrap.dedent("""
        body {
            background: pink;
        }

        div.important {
            font-weight: bold;
            font-size: 1000%;
        }
    """)
    with open(output_css) as infile:
        actual_css = infile.read()
    correct_css = re.sub(r"\s+", "", correct_css)
    actual_css = re.sub(r"\s+", "", actual_css)
    assert actual_css == correct_css
