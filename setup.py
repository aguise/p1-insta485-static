"""
EECS 485 project 1 static site generator.

Andrew DeOrio <awdeorio@umich.edu>
"""

from setuptools import setup

setup(
    name='insta485generator',
    version='0.1.0',
    packages=['insta485generator'],
    include_package_data=True,
    install_requires=[
        'bs4==0.0.1',
        'click==7.0',
        'html5validator==0.3.3',
        'jinja2==2.10.3',
        'pycodestyle==2.5.0',
        'pydocstyle==5.0.1',
        'pylint==2.4.4',
        'pytest==5.3.2',
        'requests==2.22.0',
        'sh==1.12.14',
    ],
    entry_points={
        'console_scripts': [
            'insta485generator = insta485generator.__main__:main'
        ]
    },
)
