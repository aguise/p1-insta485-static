"""Build static HTML site from directory of HTML templates and plain files."""
import json
import os
from distutils import dir_util
import click
from jinja2 import Environment
from jinja2 import PackageLoader
from jinja2 import select_autoescape


@click.command()
@click.option('-v', '--verbose', is_flag=True, help='Print more output.')
@click.argument('INPUT_DIR')
def main(input_dir, verbose):
    """Templated static website generator."""
    with open(input_dir.lstrip("/") + '/config.json', 'r') as myfile:
        data = myfile.read()

    obj = json.loads(data)
    count = 0
    for i in obj:
        template_file = i['url'] + i['template']
        context = i['context']

        env = Environment(
            loader=PackageLoader(
                'insta485generator',
                '../' + input_dir.lstrip("/") + '/templates'),
            autoescape=select_autoescape(['html', 'xml'])
        )
        template = env.get_template(i['template'])
        template_file = template.render(context)

        if os.path.exists(input_dir.lstrip("/") + "/static"):
            dir_util.copy_tree(
                input_dir.lstrip("/") + "/static",
                input_dir.lstrip("/") + "/html")
            if verbose:
                if count == 0:
                    count += 1
                    print("Copied " + input_dir + '/static -> ' + input_dir +
                          '/html')

        path = os.path.join(
            input_dir.lstrip("/"),
            "html",
            i['url'].lstrip("/"))
        if not os.path.exists(path):
            os.makedirs(path)
        with open(path + "index.html", "w") as file:
            file.write(template_file)

            if verbose:
                print(
                    "Rendered " + i['template'] + ' -> ' + path + 'index.html')


if __name__ == "__main__":
    main()
